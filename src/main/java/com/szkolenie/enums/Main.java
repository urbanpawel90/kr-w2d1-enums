package com.szkolenie.enums;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Podaj słowo: ");
        String word = sc.nextLine();

        Answer response = null;
        for (Answer possibleAnswer : Answer.values()) {
            if (possibleAnswer.isSynonym(word)) {
                response = possibleAnswer;
                break;
            }
        }
        System.out.println(response == null ?
                "Nieznana odpowiedz" :
                response);
    }
}
